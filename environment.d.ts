declare global {
  namespace NodeJS {
    interface ProcessEnv {
      USER: string;
      PASS: string;
      NODE_ENV: 'development' | 'production';
    }
  }
}

export {};
