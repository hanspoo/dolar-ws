import {
  TableContainer,
  Table,
  Tr,
  Tbody,
  Td,
  Heading,
  Box,
  Link,
  Text,
  Badge,
  Th,
  Thead,
} from '@chakra-ui/react';

import NextLink from 'next/link';

type Parameter = {
  nombre: string;
  descripción: string;
  valores: string;
  tipo: string;
  ejemplo: string;
  opcional: string;
};
type API = {
  nombre: string;
  ruta: string;
  método: 'POST' | 'GET';
  mime: 'application/json';
  parametros: Array<Parameter>;
  descripción: string;
  respuesta: string;
  ejemplos: Array<string>;
};

const moneda: Parameter = {
  nombre: 'moneda',
  descripción: 'Ejemplo: /api/hoy?moneda=USD ',
  ejemplo: 'moneda=EUR',
  tipo: 'string',
  opcional: 'Sí',
  valores: `
  EUR: Euro,
  CLF: Unidad de Fomento,
  USD: Dolar observado  
  `,
};

const hoy: API = {
  ruta: '/api/hoy',
  descripción:
    'Esta llamada retorna los valores de las monedas para el día de hoy',
  nombre: 'Valores de hoy',
  método: 'GET',
  mime: 'application/json',
  parametros: [moneda],
  respuesta: `
    [
      {
        "moneda": "USD",
        "valor": 882.33,
        "fecha": "2022-08-14"
      },
      {
        "moneda": "CLF",
        "valor": 33579.51,
        "fecha": "2022-08-14"
      },
      {
        "moneda": "EUR",
        "valor": 909.99,
        "fecha": "2022-08-14"
      }
    ]  
    `,
  ejemplos: ['api/hoy', 'api/hoy?moneda=EUR'],
};
const apis: Array<API> = [hoy];

export function ApiHelp() {
  return (
    <>
      {apis.map((api) => (
        <Api key={api.ruta} api={api} />
      ))}
    </>
  );
}

const tipo = `export interface Medicion {
  moneda: number;
  fecha: string;
  valor: number;
}
`;
const resp = `Medicion[]`;

type Campo = {
  nombre: string;
  tipo: string;
  descripcion: string;
};
const campos: Campo[] = [
  {
    nombre: 'moneda',
    tipo: 'string',
    descripcion:
      'La moneda en formato ISO 4217, se ha tomado la libertad de usar USD para dolar observado.',
  },
  {
    nombre: 'fecha',
    tipo: 'string',
    descripcion:
      'La fecha formato ISO, calendario oficial de chile: YYYY-MM-DD, facilmente parseable por todos los sistemas.',
  },
  {
    nombre: 'valor',
    tipo: 'float',
    descripcion: 'Valor del indicador',
  },
];

function Api({ api }: { api: API }) {
  return (
    <Box w="100%" color="black">
      <Heading mb="2" size="lg">
        {api.nombre}
      </Heading>
      <Text color="#999" mb="3">
        {api.descripción}
      </Text>

      <Box mb="6">
        <Badge p="2">{api.método}</Badge> {api.ruta}
        <Link ml="14" size="sm">
          <NextLink href="/api/hoy">Probar</NextLink>
        </Link>
      </Box>
      <Heading mb="2" size="xs">
        Parámetros
      </Heading>
      <TableContainer mb="4">
        <Table className="params">
          <Thead>
            <Tr>
              <Th>Parámetro</Th>
              <Th>Tipo</Th>
              <Th>Valores</Th>
              <Th>Descripción</Th>
            </Tr>
          </Thead>
          <Tbody>
            {api.parametros.map(({ nombre, valores, tipo, descripción }) => (
              <Tr key={nombre}>
                <Td>{nombre}</Td>
                <Td>{tipo}</Td>
                <Td>
                  <pre>{valores}</pre>
                </Td>
                <Td>{descripción}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Heading mb="2" size="xs">
        Respuesta
      </Heading>
      <Text mt="4" mb="4">
        La respuesta siempre es un array de mediciones: <pre>{resp}</pre>
      </Text>

      <Text mb="1">
        Cada medición en typescript es:<pre>{tipo}</pre>
      </Text>

      <Text mt="4">
        {campos.map(({ nombre, tipo, descripcion }) => (
          <div key={nombre}>
            <small>{nombre}</small>
            <small>
              <Text color="#999">{descripcion}</Text>
            </small>
          </div>
        ))}
      </Text>

      <Text mt="6">Ejemplo:</Text>

      <pre>{api.respuesta}</pre>
    </Box>
  );
}
