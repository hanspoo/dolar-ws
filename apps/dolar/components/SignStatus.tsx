import { signIn, signOut } from 'next-auth/react';
import styles from './header.module.css';

export type PropsSignStatus = {
  session: unknown;
  loading: boolean;
};

export function SignStatus({ session, loading }: PropsSignStatus) {
  return (
    <>
      {!session && (
        <a
          href={`/api/auth/signin`}
          onClick={(e) => {
            e.preventDefault();
            signIn();
          }}
        >
          Login
        </a>
      )}
      {session?.user && (
        <>
          {session.user.image && (
            <span
              style={{ backgroundImage: `url('${session.user.image}')` }}
              className={styles.avatar}
            />
          )}

          <strong style={{ marginRight: '0.5em' }}>
            Hola {session.user.email ?? session.user.name}
          </strong>

          <a
            href={`/api/auth/signout`}
            onClick={(e) => {
              e.preventDefault();
              signOut();
            }}
          >
            Salir
          </a>
        </>
      )}
    </>
  );
}
