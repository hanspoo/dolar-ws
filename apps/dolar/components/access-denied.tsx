import { Text, Heading, Link } from '@chakra-ui/react';
import { signIn } from 'next-auth/react';

export default function AccessDenied() {
  return (
    <>
      <Heading>Página protegida</Heading>
      <Text my="4">
        <Link
          href="/api/auth/signin"
          onClick={(e) => {
            e.preventDefault();
            signIn();
          }}
        >
          Haga click para iniciar sesión
        </Link>
      </Text>
    </>
  );
}
