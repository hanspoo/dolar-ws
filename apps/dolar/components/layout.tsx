import Header from './header';
import Footer from './footer';
import { Box } from '@chakra-ui/react';

interface Props {
  children: React.ReactNode;
}

export default function Layout({ children }: Props) {
  return (
    <Box w="100%" p="4">
      <Header />
      <main>{children}</main>
      <Footer />
    </Box>
  );
}
