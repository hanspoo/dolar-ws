import { useSession } from 'next-auth/react';
import Link from 'next/link';
import { SignStatus } from './SignStatus';

export default function Footer() {
  const { data: session, status } = useSession();
  const loading = status === 'loading';

  return (
    <>
      <hr style={{ margin: '1em 0' }} />
      <Link href="/">Home</Link>
      {' | '}
      <Link href="/protected">Administración</Link>
      {' | '}
      <SignStatus session={session} loading={loading} />
    </>
  );
}
