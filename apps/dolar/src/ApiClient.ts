import axios from 'axios';
import { getSession } from 'next-auth/react';

// const baseURL = process.env.SOME_API_URL || 'http://localhost:1337';

const ApiClient = axios.create();

ApiClient.interceptors.request.use(async (request) => {
  const session = await getSession();
  if (session) {
    request.headers.Authorization = `Bearer ${session.jwt}`;
  }
  return request;
});

ApiClient.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    console.log(`error`, error);
  }
);

export default ApiClient;
