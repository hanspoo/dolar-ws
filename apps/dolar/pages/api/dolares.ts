import type { NextApiRequest, NextApiResponse } from 'next';
import { ultimoMes } from '@dolar-ws/dolar-cache';
import { removeMongo } from './removeMongo';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const dolares = await ultimoMes();
  res.send(removeMongo(dolares));
}
