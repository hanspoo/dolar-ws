import NextAuth, { NextAuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

/**
 * Por el momento controlamos acceso a zona de administración con
 * un usuario y contraseñas almacenados en archivo .env.
 */
export const authOptions: NextAuthOptions = {
  secret: process.env.AUTH_SECRET,
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        username: { label: 'Username', type: 'text', placeholder: 'username' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials, req) {
        const userValid = credentials.username === process.env.USERNAME;
        const passValid = credentials.password === process.env.PASSWORD;

        if (userValid && passValid) {
          const user = {
            id: 1,
            name: process.env.USERNAME,
          };
          return user;
        } else {
          return null;
        }
      },
    }),
  ],
  theme: {
    colorScheme: 'light',
  },
  callbacks: {
    async jwt({ token }) {
      token.userRole = 'admin';
      return token;
    },
  },
};

export default NextAuth(authOptions);
