import { unstable_getServerSession } from 'next-auth/next';
import { authOptions } from '../auth/[...nextauth]';

import type { NextApiRequest, NextApiResponse } from 'next';
import { updateMesCache } from '@dolar-ws/dolar-cache';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {

  const session = await unstable_getServerSession(req, res, authOptions);

  if (!session) throw Error('No hay sesión, llamada inválida');

  await updateMesCache();

  res.send({
    msg: 'Listo',
  });
}
