import { Dolar, monedas } from '@dolar-ws/scrap';

const mapa: Record<number, string> = monedas.reduce((acc, iter) => {
  acc[iter.id] = iter.iso;
  return acc;
}, {});

export function removeMongo(dolares: Dolar[]): unknown {
  return dolares.map(({ moneda, valor, fecha }) => ({
    moneda: mapa[moneda],
    valor,
    fecha,
  }));
}
