import type { NextApiRequest, NextApiResponse } from 'next';
import { dolarHoy } from '@dolar-ws/dolar-cache';
import { removeMongo } from './removeMongo';
import { IsoMoneda } from '@dolar-ws/scrap';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { moneda } = req.query;
  const dolar = await dolarHoy(moneda as unknown as IsoMoneda);
  res.send(removeMongo(dolar));
}
