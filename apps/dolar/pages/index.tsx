import { chakra, Box, Heading, Text as ChakraText } from '@chakra-ui/react';
import { ApiHelp } from '../components/apiHelp';
import Layout from '../components/layout';

const Text = chakra(ChakraText, { baseStyle: { py: 4 } });
export default function IndexPage() {
  return (
    <Layout>
      <Heading mt="4" as="h1" size="xl">
        API REST de indicadores financieros
      </Heading>
      <Text>
        Ofrecemos un API Rest de los indicadores financieros más importantes en
        Chile:
      </Text>
      <Heading as="h3" size="l">
        - Dolar Observado
      </Heading>
      <Heading as="h3" size="l">
        - UF
      </Heading>
      <Heading as="h3" size="l">
        - Euro
      </Heading>
      <Heading as="h2" my="10">
        Endpoints
      </Heading>

      <ApiHelp />

      <Box w="100%" color="black">
        <Text>/api/semana y /api/mes funcionan de la misma manera</Text>
      </Box>

      <Heading mt="1em" as="h2">
        Porqué lo hicimos?
      </Heading>
      <Text>Homogenizar la información para simplificar el uso.</Text>
      <Heading as="h3" size="lg">
        Espacios
      </Heading>
      <Text>
        El servicio de banco central no tiene valores para todos los días, hay
        hoyos de uno o más días, en los cuales el valor es el string NaN, Not a
        Number. Estaría mejor un tipo aceptado por JSON como undefined o null.
        En estos casos hemos decidir ofrecer algo más sensato y completar con el
        valor del último día con datos válidos a esa fecha.
      </Text>
      <Heading as="h3" size="lg">
        Dias faltantes
      </Heading>
      <Text>
        En particular para el dólar dólar observado, encontramos que el día
        sabados no hay valor para el dólar observado, ni siquiera viene el día
        en la serie. En estos casos, agregamos el día y lo completamos con el
        último valor registrado, para poder ofrecer la serie completa.
      </Text>
    </Layout>
  );
}
