import { useState } from 'react';
import { useSession } from 'next-auth/react';
import Layout from '../components/layout';
import AccessDenied from '../components/access-denied';
import { Button, Heading, Text } from '@chakra-ui/react';
import ApiClient from '../src/ApiClient';

type Msg = {
  msg: string;
};
export default function ProtectedPage() {
  const { data: session, status } = useSession();
  const loading = status === 'loading';
  const [respuesta, setRespuesta] = useState<Msg>();

  const [actualizando, setActualizando] = useState(false);

  // Fetch content from protected route

  const actualizar = async () => {
    setActualizando(true);

    const res = await ApiClient('/api/examples/actualizar', { method: 'POST' });
    const json = res.data;
    if (json) {
      setActualizando(false);
      setRespuesta(json);
      setTimeout(() => setRespuesta(undefined), 3000);
    }
  };

  // When rendering client side don't display anything until loading is complete
  if (typeof window !== 'undefined' && loading) return null;

  // If no session exists, display access denied message
  if (!session) {
    return (
      <Layout>
        <AccessDenied />
      </Layout>
    );
  }

  // If session exists, display content
  return (
    <Layout>
      <Heading mb="4">Administración</Heading>

      <Heading size="lg" mb="2">
        Actualizar
      </Heading>
      <Text mb="2">Actualiza los indicadores desde el banco central</Text>
      {respuesta ? (
        <p>{respuesta.msg}</p>
      ) : (
        <Button onClick={actualizar} mb="2">
          {actualizando ? 'Actualizando...' : 'Enviar'}
        </Button>
      )}
    </Layout>
  );
}
