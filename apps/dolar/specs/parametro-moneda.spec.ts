import { closeMongo, connectMongo } from '@dolar-ws/dolar-cache';

import { testApiHandler } from 'next-test-api-route-handler';
import hoy from '../pages/api/hoy';

beforeAll(async () => {
  await connectMongo();
});

afterAll(async () => {
  await closeMongo();
});

describe('prueba api hoy', () => {
  it('handles multiple set-cookie headers', async () => {
    expect.hasAssertions();

    await testApiHandler({
      handler: hoy,
      test: async ({ fetch }) => {
        expect((await fetch()).status).toBe(200);
        await expect((await fetch()).json()).resolves.toStrictEqual([]);
      },
    });
  });
});
