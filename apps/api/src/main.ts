/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
import 'dotenv/config';
import {
  connectMongo,
  dolarHoy,
  ultimoMes,
  updateMesCache,
} from '@dolar-ws/dolar-cache';
import express from 'express';
import cors from 'cors';
import logger from './logger';

const app = express();
app.use(cors());
app.use(logger);

connectMongo();

app.get('/api', (req, res) => {
  res.send({ message: 'Bienvenido al api de dolar observado!' });
});
app.get('/api/dolar', async (req, res) => {
  const dolar = await dolarHoy();
  res.send(dolar);
});
app.get('/api/actualizar', async (req, res) => {
  await updateMesCache();
  const dolar = await dolarHoy();
  res.send(dolar);
});
app.get('/api/dolares', async (req, res) => {
  const dolares = await ultimoMes();
  res.send(dolares);
});

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/api`);
});
server.on('error', console.error);
