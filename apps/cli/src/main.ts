import dotenv from 'dotenv';
dotenv.config();
import log from 'why-is-node-running';
import wtf = require('wtfnode');
import {
  closeMongo,
  connectMongo,
  updateMesCache,
} from '@dolar-ws/dolar-cache';

(async function dale() {
  //connectMongo();
  await updateMesCache();
  await closeMongo();

  // log();
  // wtf.dump();
})();
