# dolar-ws

Acceder a dolar observado del banco central

Diariamente actualiza su base de datos a partir del banco central y expone un API Rest de fácil consumo.

El API REST lee desde mongo.

## Prerequisitos

### nodejs

Instalar nodejs para el sistema operativo

### pm2

En este caso vamos a desplegar con pm2 que nos entrega alta disponibilidad, lo instalamos

```
npm i -g pm2
```

### mongodb

Instalar mongodb, en ubuntu:

```
sudo apt install mongodb
```

## Clonar

```
git clone https://gitlab.com/hanspoo/dolar-ws
npm install
npm run build
cd dist/apps/api/
```

## El archivo .env debe llevar las credenciales del banco central

```
cat <<EOF> dist/apps/api/.env

USER_BC=99698888
PASS_BC="#$%&/(%$&"
MONGO_URI=mongodb://127.0.0.1:27017/dinero
EOF
```

Hay que registrarse en banco central para usar el API REST y mandan las credenciales por mail.

## Iniciar con pm2

```
pm2 start main.js --name dolar
```
