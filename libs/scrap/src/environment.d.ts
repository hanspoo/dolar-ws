declare global {
  namespace NodeJS {
    interface ProcessEnv {
      USER_BC: string;
      PASS_BC: string;
      NODE_ENV: 'dev' | 'prod';
    }
  }
}

export {};
