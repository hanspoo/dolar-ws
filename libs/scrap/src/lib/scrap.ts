import axios from 'axios';
import moment from 'moment';
import { RespuestaBancoCentral } from './RespuestaBancoCentral';

const user = process.env.USER_BC;
const pass = process.env.PASS_BC;

export class Moneda {
  constructor(
    public id: number,
    public nombre: string,
    public iso: string,
    public serie: string
  ) {}

  scrap(): Promise<Array<Dolar>> {
    const fecha = new Date();
    return this.scrapRange(fecha, fecha);
  }

  async scrapRange(
    desde: Date | string,
    hasta: Date | string
  ): Promise<Array<Dolar>> {
    if (typeof desde === 'string') {
      if (/^\d\d-/.test(desde))
        throw Error('Fecha desde debe venir en formato iso YYYY-MM-DD');
    }
    if (typeof hasta === 'string') {
      if (/^\d\d-/.test(hasta))
        throw Error('Fecha hasta debe venir en formato iso YYYY-MM-DD');
    }

    const from =
      typeof desde === 'string' ? desde : desde.toISOString().substring(0, 10);
    const to =
      typeof hasta === 'string' ? hasta : hasta.toISOString().substring(0, 10);

    const url = `http://si3.bcentral.cl/SieteRestWS/SieteRestWS.ashx?user=${user}&pass=${pass}&firstdate=${from}&lastdate=${to}&timeseries=${this.serie}&function=GetSeries`;

    const response = await axios.get<RespuestaBancoCentral>(url);

    if (response.data.Codigo !== 0) {
      throw Error(response.data.Descripcion);
    }

    let respuesta = fillGaps(this.parseResponse(response.data));

    const hoy = moment().format('YYYY-MM-DD');
    const hoyFormatChile = moment().format('DD-MM-YYYY');
    if (to >= hoy) {
      const dolarHoy = respuesta.find((d) => d.fecha === hoyFormatChile);
      if (!dolarHoy) {
        respuesta = completarHastaHoyConUltimoValido(respuesta);
      }
    }

    // REMOVER:
    // Validar que todas las fechas estén en el formato ISO
    respuesta.forEach((dolar) => {
      if (!/^\d\d\d\d-\d\d-\d\d/.test(dolar.fecha)) {
        throw Error(JSON.stringify(dolar) + ' no viene en formato ISO');
      }
    });

    return respuesta;
  }

  parseResponse(data: RespuestaBancoCentral): Array<Dolar> {
    const {
      Series: { Obs },
    } = data;
    return Obs.map((obs) => ({
      moneda: this.id,
      fecha: fixeaFecha(obs.indexDateString),
      valor: /^\d+/.test(obs.value) ? parseFloat(obs.value) : undefined,
    }));
  }
}

function fillGaps(list: Array<Dolar>): Array<Dolar> {
  // El primer elemento del scrap debe tener valor, de lo contrario no sabremos
  // con que valor rellenar

  const i = list.findIndex((s) => s.valor !== undefined);

  return list.slice(i).reduce((acc: Array<Dolar>, current: Dolar) => {
    // Si un valores es nulo, usar el anterior

    if (current.valor === undefined) {
      current.valor = acc[acc.length - 1].valor;
    }
    return acc.concat(current);
  }, []);
}

export interface Dolar {
  moneda: number;
  fecha: string;
  valor?: number;
}

const REGEX_FORMATO_CHILE = /^(\d\d)-(\d\d)-(\d\d\d\d)/;

function stringAsMoment(s: string): moment.Moment {
  const match = REGEX_FORMATO_CHILE.exec(s);
  const fecha = match ? `${match[3]}-${match[2]}-${match[1]}` : s;

  return moment(fecha, 'YYYY-MM-DD');
}

export interface Observado {
  indexDateString: string;
  value: string;
  statusCode: string;
}
function fixeaFecha(s: string): string {
  const match = /(\d\d)-(\d\d)-(\d\d\d\d)/.exec(s);
  if (match) {
    return `${match[3]}-${match[2]}-${match[1]}`;
  } else {
    return s;
  }
}

function completarHastaHoyConUltimoValido(respuesta: Dolar[]): Dolar[] {
  // buscar el último valido

  let ultimoValido: Dolar | undefined;

  ITER: for (let i = respuesta.length - 1; i >= 0; i--) {
    const dolar = respuesta[i];
    if (dolar.valor) {
      ultimoValido = dolar;
      break ITER;
    }
  }

  if (ultimoValido) {
    // Agregar los que faltan hasta hoy
    const fecha: moment.Moment = stringAsMoment(ultimoValido.fecha);
    const manana = fecha.clone();
    manana.add(1, 'days');

    const hoy = moment();
    while (manana <= hoy) {
      respuesta.push({ ...ultimoValido, fecha: manana.format('YYYY-MM-DD') });
      manana.add(1, 'days');
    }
  }

  return respuesta;
}
