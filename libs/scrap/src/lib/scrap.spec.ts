/**
 * @jest-environment node
 */

import nock from 'nock';
import dolares from '../dolar-jul-2022.json';
import { dolar } from './models/Moneda';

describe('descarga la base del ultimo mes', () => {
  it('debe tener al menos 10 registros', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);

    const list = await dolar.scrap();
    expect(list.length).toBeGreaterThan(10);
  });
  it('debe ser 932.08 el 1 de julio de 2022', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);

    const list = await dolar.scrap();
    expect(list[0].valor).toBe(932.08);
  });
  it('el primer registro es 1 de julio', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);

    const list = await dolar.scrap();
    expect(list[0].fecha).toBe('2022-07-01');
  });
  it('scrap soporta rango de fechas julio 2022', async () => {
    nock('https://si3.bcentral.cl')
      .get(/.*firstdate=2022-07-01&lastdate=2022-07-20.*/)
      .reply(200, dolares);

    const desde = new Date('2022-07-01');
    const hasta = new Date('2022-07-20');

    await dolar.scrapRange(desde, hasta);
  });
  it('scrap soporta rango de fechas junio 2022', async () => {
    nock('https://si3.bcentral.cl')
      .get(/.*firstdate=2022-06-01&lastdate=2022-06-30.*/)
      .reply(200, dolares);

    const desde = new Date('2022-06-01');
    const hasta = new Date('2022-06-30');

    await dolar.scrapRange(desde, hasta);
  });
});

const dolarsPrimerosNulos = [
  { fecha: '02-07-2022', valor: undefined },
  { fecha: '03-07-2022', valor: undefined },
  { fecha: '04-07-2022', valor: 934.54 },
  { fecha: '05-07-2022', valor: undefined },
  { fecha: '06-07-2022', valor: 948.51 },
  { fecha: '18-07-2022', valor: undefined },
];
const dolarsFixed = [
  { fecha: '04-07-2022', valor: 934.54 },
  { fecha: '05-07-2022', valor: 934.54 },
  { fecha: '06-07-2022', valor: 948.51 },
  { fecha: '18-07-2022', valor: 948.51 },
];

describe('fixea el scrap, rellena los dias sin datos', () => {
  it('no deben quedar valores nulos', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);

    const dolars = await dolar.scrap();

    expect(dolars.filter((s) => s.valor !== undefined).length).toBe(
      dolars.length
    );
  });
});
