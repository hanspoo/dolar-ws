import { Observado } from './scrap';

export interface RespuestaBancoCentral {
  Codigo: number;
  Descripcion: string;
  Series: {
    descripEsp: string;
    descripIng: string;
    seriesId: string;
    Obs: Observado[];
  };
  SeriesInfos: any[];
}
