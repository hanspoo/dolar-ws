import { Moneda } from '../scrap';

export type IsoMoneda = 'CLF' | 'EUR' | 'USD';
export const dolar = new Moneda(
  1,
  'Dolar Observado',
  'USD',
  'F073.TCO.PRE.Z.D'
);
export const uf = new Moneda(2, 'Unidad de Fomento', 'CLF', 'F073.UFF.PRE.Z.D');
export const euro = new Moneda(3, 'Euro', 'EUR', 'F072.CLP.EUR.N.O.D');

const monedas = [dolar, uf, euro];
export { monedas };
