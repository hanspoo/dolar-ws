/**
 * @jest-environment node
 */

import nock from 'nock';
import moment from 'moment';
import { RespuestaBancoCentral } from './RespuestaBancoCentral';
import { dolar } from './models/Moneda';

const template: RespuestaBancoCentral = {
  Codigo: 0,
  Descripcion: 'Success',
  Series: {
    descripEsp: 'Tipo de cambio del dólar observado diario',
    descripIng: 'Observed dollar daily exchange rate',
    seriesId: 'F073.TCO.PRE.Z.D',
    Obs: [],
  },
  SeriesInfos: [],
};

describe('rellenar días faltantes', () => {
  test('si dia actual en scrap y no tiene datos, completar con anterior ', async () => {
    // Ej 1: Banco envia datos hasta ayer

    const dolares = { ...template };
    const ayer = moment().subtract(1, 'days');
    const manana = moment().add(1, 'days');
    dolares.Series.Obs = [
      {
        indexDateString: ayer.format('DD-MM-YYYY'),
        value: '940.9',
        statusCode: 'OK',
      },
    ];

    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);
    const datos = await dolar.scrapRange(ayer.toDate(), manana.toDate());
    expect(datos.length).toBe(2);
    expect(datos[1].valor).toBe(940.9);
  });
  it('llamo el mismo ultimo día, un registro', async () => {
    // Ej 1: Banco envia datos hasta ayer

    const dolares = { ...template };
    dolares.Series.Obs = [
      {
        indexDateString: moment().format('DD-MM-YYYY'),
        value: '940.9',
        statusCode: 'OK',
      },
    ];

    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);
    const datos = await dolar.scrapRange(moment().toDate(), moment().toDate());
    expect(datos.length).toBe(1);
  });
});
