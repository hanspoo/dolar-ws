# scrap

Baja los datos de dolar observado del banco central
los deja en una base de datos mongodb.

## Algoritmo

revisar datos actuales almacenados
determinar el rango de fechas faltante, máximo un mes
descargar base de datos del pendiente a la fecha
actualizar base de datos local

# Motor de scrap

El motor de scrap ejecuta con cierta frecuencia y mantiene un cache
en mongodb con los valores obtenidos.

# Motor de consulta

La consulta consulta directamente el cache y responde en formato json.

# El día después

El día siguiente normalmente se puede obtener luego de las 20:00.
