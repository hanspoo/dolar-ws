# Base mongo con resultado

Este servicio ejecuta el scrap y guarda la info en un cache local basado en mongo

## mongo shell

```
julian@urantia ~ $ mongo
MongoDB shell version v3.6.8
...
> use dinero
switched to db dinero

> db.dolars.find()
{ "_id" : ObjectId("62dfeb38554e63992b0a56ca"), "fecha" : "01-07-2022", "valor" : 932.08, "__v" : 0 }
{ "_id" : ObjectId("62dfeb38554e63992b0a56cb"), "fecha" : "02-07-2022", "valor" : 932.08, "__v" : 0 }
...
```
