/* eslint-disable @typescript-eslint/no-var-requires */
global.TextEncoder = require('util').TextEncoder;
global.TextDecoder = require('util').TextDecoder;
import moment from 'moment';
import nock from 'nock';
import dolares from './dolar-jul-2022.json';
import { ultimaSemana, updateCache } from '../src/lib/dolar-cache';
import DolarModel from '../src/models/dolar';
import connectMongo, { closeMongo } from '../src/lib/connectMongo';
import mongoose from 'mongoose';

const dolares12 = {
  Codigo: 0,
  Descripcion: 'Success',
  Series: {
    descripEsp: 'Tipo de cambio del dólar observado diario',
    descripIng: 'Observed dollar daily exchange rate',
    seriesId: 'F073.TCO.PRE.Z.D',
    Obs: [
      {
        indexDateString: '01-07-2022',
        value: '932.08',
        statusCode: 'OK',
      },
      {
        indexDateString: '02-07-2022',
        value: 'NaN',
        statusCode: 'ND',
      },
    ],
  },
};

beforeAll(async () => {
  await connectMongo();
});
beforeEach(async () => {
  await DolarModel.deleteMany({});
});
afterAll(async () => {
  await closeMongo();
  // await DolarModel.deleteMany({});
});

describe('dolarCache', () => {
  it('cuando parte el test debe haber 0 registros', async () => {
    expect(await DolarModel.count()).toBe(0);
  });
  it('debe tener 19 registros desde scrap con nock', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);
    await updateCache('2022-07-01');
    expect(await DolarModel.count()).toBe(19);
  });
  it('no debe caer cuando hay duplicados', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);
    await updateCache('2022-07-01');
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);
    await updateCache('2022-07-01');
    expect(await DolarModel.count()).toBe(19);
  });

  it('el 2 de julio sin datos, debe tener el valor del 1', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares12);
    await updateCache('2022-07-01');
    const dos = await DolarModel.findOne({ fecha: '2022-07-02' });
    expect(dos.valor).toBe(932.08);
  });
  it('debe traer la última semana', async () => {
    nock('https://si3.bcentral.cl').get(/.*/).reply(200, dolares);
    await updateCache('2022-07-01');
    const list = await ultimaSemana();
    expect(list.length).toBe(7);
  });
});

/**
 * Como probar la validación de duplicados.
 *
 * Borrar todo
 * Insertar los dolares debe entregar 28 insertados
 * Insertar los dolares nuevamente 0 insertados
 *
 */

describe('utilies para generar fechas', () => {
  it('obtiene dia inicio para ultima seman', () => {
    const [from, _] = fechasUltimaSemana();
    const desde = moment().subtract(7, 'days');
    expect(from).toBe(desde.format('YYYY-MM-DD'));
  });
  it('obtiene dia termino para ultima seman', () => {
    const [_, to] = fechasUltimaSemana();
    expect(to).toBe(moment().format('YYYY-MM-DD'));
  });
});

function fechasUltimaSemana(): [string, string] {
  const desde = moment().subtract(7, 'days');
  return [desde.format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')];
}
