const hoyISO = new Date().toISOString().substring(0, 10);
const dolaresHoy: Dolar[] = [
  {
    moneda: 1,
    valor: 945.35,
    fecha: '2022-08-22',
  },
  {
    moneda: 2,
    valor: 949.53,
    fecha: '2022-08-22',
  },
  {
    moneda: 3,
    valor: 33700.21,
    fecha: '2022-08-22',
  },
].map((med) => ({ ...med, fecha: hoyISO }));

// beforeAll(async () => {
//   await connectMongo();
// });
// afterAll(async () => {
//   await mongoose.connection.close();
// });
// describe('Prueba búsqueda por código de moneda', () => {
//   it('Debe entregar todas las monedas sin filtro', async () => {
//     const res = await dolarHoy();
//     expect(res).toEqual(results);
//   });
// });

/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { Dolar } from '@dolar-ws/scrap';

import DolarModel from '../src/models/dolar';
import mongoose from 'mongoose';
import { dolarHoy } from '../src/lib/dolar-cache';

describe('búsqueda por moneda en /hoy', () => {
  // This is an Example test, do not merge it with others and do not delete this file
  beforeEach(async () => {
    await mongoose.connect('mongodb://127.0.0.1/conejo');
    await DolarModel.collection.drop();
  });

  afterEach(async () => {
    await mongoose.disconnect();
  });
  it('crea un objeto Medicion', async () => {
    const newTodo = await new DolarModel(dolaresHoy[0]);
    await newTodo.save();
    expect(newTodo._id).toBeDefined();
  });
  it('el día de hoy recupera los 3 indicadores', async () => {
    await DolarModel.insertMany(dolaresHoy);

    const dolars = await dolarHoy();
    expect(dolars.length).toBe(3);
  });
  it('si paso la moneda, debe venir un sólo valor', async () => {
    await DolarModel.insertMany(dolaresHoy);

    const dolars = await dolarHoy('EUR');
    expect(dolars.length).toBe(1);
  });
});
