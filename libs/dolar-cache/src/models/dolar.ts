import { Dolar } from '@dolar-ws/scrap';
import mongoose from 'mongoose';
import { Schema, model, models } from 'mongoose';
mongoose.set('debug', true);

const DolarSchema = new Schema({
  moneda: Number,
  fecha: {
    type: String,
    validate: {
      validator: function (v: string) {
        return /\d{4}-\d{2}-\d{2}/.test(v);
      },
      message: (props: any) => `${props.value} no tiene formato yyyy-mm-dd`,
    },
    required: [true, 'La fecha es obligatoria'],
  },

  valor: Number,
});

// DolarSchema.index({ moneda: 1, fecha: 1 }, { unique: true });

const DolarModel = models['Dolar'] || model<Dolar>('Dolar', DolarSchema);

export default DolarModel;
