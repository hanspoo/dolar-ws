declare global {
  namespace NodeJS {
    interface ProcessEnv {
      MONGO_URI: string;
      USER_BC: string;
      PASS_BC: string;
    }
  }
}

export {};
