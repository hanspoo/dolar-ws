import moment from 'moment';

import DolarModel from '../models/dolar';

import { dolar, Dolar, IsoMoneda, Moneda, monedas } from '@dolar-ws/scrap';

export function dolarCache(): string {
  return 'dolar-cache';
}

export async function deleteDatabase() {
  await DolarModel.deleteMany({});
}

export async function dolarFor(fecha: string): Promise<Dolar> {
  const dolares = <Dolar[]>await DolarModel.find({ fecha });
  if (!dolares) throw Error('No hay dolar para ' + fecha);
  return dolares[0];
}
export async function dolarHoy(monedaISO?: IsoMoneda): Promise<Dolar[]> {
  const fecha = moment().format('YYYY-MM-DD');

  let dolares: Dolar[];
  if (monedaISO) {
    const moneda = monedas.find((m) => m.iso === monedaISO);
    if (!moneda) {
      throw Error('Código ISO de moneda inválido ' + monedaISO);
    }
    dolares = <Dolar[]>await DolarModel.find({ fecha, moneda: moneda.id });
  } else {
    dolares = <Dolar[]>await DolarModel.find({ fecha });
  }
  if (!dolares) throw Error('No hay dolar para ' + fecha);
  return dolares;
}
export async function ultimaSemana(): Promise<Array<Dolar>> {
  const dolares = <Dolar[]>await DolarModel.find().sort({ fecha: -1 }).limit(7);

  if (!dolares) throw Error('No hay dolar para ');
  return dolares;
}
export async function ultimoMes(): Promise<Array<Dolar>> {
  const dolares = <Dolar[]>(
    await DolarModel.find().sort({ fecha: -1 }).limit(30)
  );

  if (!dolares) throw Error('No hay dolar para ');
  return dolares;
}

export async function updateCache(date?: string) {
  if (!date) date = moment().format('YYYY-MM-DD');
  const dolares = await dolar.scrapRange(date, date);
  const promesas = (await sinDuplicados(dolares)).map((d) =>
    DolarModel.create(d)
  );
  await Promise.all(promesas);
}
export async function updateMesCache() {
  const desde = moment().subtract(30, 'day').format('YYYY-MM-DD');
  const hasta = moment().add(1, 'day').format('YYYY-MM-DD');

  const promesas = monedas.map(async (moneda) => {
    return actualizarMoneda(moneda, desde, hasta);
  });

  await Promise.all(promesas);
}

async function actualizarMoneda(moneda: Moneda, desde: string, hasta: string) {
  const dolares = await moneda.scrapRange(desde, hasta);

  const lista = await sinDuplicados(dolares);
  const promesas = lista.map((d) => {
    return DolarModel.create(d);
  });
  await Promise.all(promesas);
}

async function sinDuplicados(dolares: Dolar[]): Promise<Dolar[]> {
  const fechas = (await DolarModel.find()).reduce(function (
    acc: Record<string, boolean>,
    current: Dolar
  ) {
    acc[`${current.moneda}-${current.fecha}`] = true;
    return acc;
  },
  {});
  return dolares.filter((dolar) => !fechas[`${dolar.moneda}-${dolar.fecha}`]);
}
