import mongoose from 'mongoose';

let db: typeof mongoose;

const uri = process.env.MONGO_URI || 'mongodb://127.0.0.1:27017/dinero';
if (!uri) throw Error('No hay url de mongo');

const connectMongo = async () => {
  // db = await mongoose.connect(uri, { loggerLevel: 'debug' });
  // mongoose.connection.on(
  //   'error',
  //   console.error.bind(console, 'MongoDB connection error:')
  // );
  // mongoose.connection.on(
  //   'disconnecting',
  //   console.error.bind(console, 'Desconectando de mongoose')
  // );
};

mongoose.connect(uri, { loggerLevel: 'debug' });

export { connectMongo };
export default connectMongo;
const closeMongo = async (): Promise<void> => {
  // db.connection.close(true);

  // else {
  // await db.disconnect();
  // await db.connection.close(true);
  return mongoose.disconnect();
  // }
};
export { closeMongo };
